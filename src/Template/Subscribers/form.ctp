<div class="container">
        <?php echo $this->Html->link(__('Prisijungti'), ['controller' => 'Users', 'action' => 'login']); ?>
    <div class="jumbotron">
        <h3>Prenumeruok mūsų <span class="label label-default">Naujienas</span></h3>
        <ol class="breadcrumb">
            <li><?php echo $this->Html->link(__('Titulinis'), ['controller' => 'Subscribers', 'action' => 'form', 'home']); ?></li>
            <li class="active">Naujienų prenumerata</li>
        </ol>
        <?php echo $this->Form->create($newsletter); ?>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <?php echo $this->Form->input('name', ['label' => __('Jūsų vardas'), 'class' => 'form-control', 'id' => 'name', 'placeholder' => __('Jūsų vardas'), 'maxlength' => '100', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>', 'inputContainerError' => '<div class="form-group has-error">{{content}}{{error}}</div>']]); ?>
            </div>
            <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->input('email', ['label' => __('Elektroninio pašto adresas'), 'class' => 'form-control', 'id' => 'email', 'placeholder' => __('Elektroninio pašto adresas'), 'maxlength' => '100', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>', 'inputContainerError' => '<div class="form-group has-error">{{content}}{{error}}</div>']]); ?>
            </div>
        </div>
        <div class="row <?php if (isset($catError)) echo 'has-error'; ?>">
            <?php for($i = 0; $i < sizeof($categories); $i++) { ?>
            <div class="col-sm-2 col-xs-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="category[]" value="<?php echo $categories[$i]; ?>"> <?php echo $categories[$i]; ?>
                    </label>
                </div>
            </div>
            <?php } ?>
        </div>
            <?php if (isset($catError)) { ?>
            <div class="error-message">Reikia pasirinkti bent vieną kategoriją.</div>
            <?php } ?>
        <div class="row">
            <div class="col-sm-2 col-xs-4">
                <?php echo $this->Form->submit(__('Patvirtinti'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
