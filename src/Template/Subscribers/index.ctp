<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><?= $this->Html->link(__('Naujienų prenumeratoriai'), ['controller' => 'Subscribers', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('Naujienų kategorijos'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
            </ul>
        </div>
        <div class="col-sm-9 col-xs-12">
            <h3><?= __('Naujienų prenumeratoriai') ?></h3>
            <table class="table table-striped">
                <tr>
                    <th><?php echo $this->Html->link(__('Vardas'), ['controller' => 'Subscribers', 'action' => 'index', 'name']); ?></th>
                    <th><?php echo $this->Html->link(__('Elektroninis paštas'), ['controller' => 'Subscribers', 'action' => 'index', 'email']); ?></th>
                    <th><?php echo $this->Html->link(__('Data'), ['controller' => 'Subscribers', 'action' => 'index', 'date']); ?></th>
                </tr>
                <?php for($i = 0; $i < sizeof($subscribers); $i++) { ?>
                    <tr>
                        <td><?php echo $subscribers[$i]['name']; ?></td>
                        <td><?php echo $subscribers[$i]['email']; ?></td>
                        <td><?php echo $subscribers[$i]['date']; ?></td>
                        <td class="row">
                            <?php for($j = 0; $j < sizeof($subscribers[$i]['categories']['name']); $j++) { ?>
                                <input class="btn btn-default" type="button" value="<?php echo $subscribers[$i]['categories']['name'][$j]; ?>">
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
