<div class="users form">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Įveskite prisijungimo vardą ir slaptažodį') ?></legend>
        <?= $this->Form->input('username', ['label' => 'Prisijungimo vardas']) ?>
        <?= $this->Form->input('password', ['label' => 'Slaptažodis']) ?>
    </fieldset>
<?= $this->Form->button(__('Prisijungti')); ?>
<?= $this->Form->end() ?>
</div>
