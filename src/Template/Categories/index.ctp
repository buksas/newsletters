<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <ul class="nav nav-pills nav-stacked">
                <li><?= $this->Html->link(__('Naujienų prenumeratoriai'), ['controller' => 'Subscribers', 'action' => 'index']) ?></li>
                <li class="active"><?= $this->Html->link(__('Naujienų kategorijos'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
            </ul>
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3><?= __('Pridėti kategoriją') ?></h3>
            <?php echo $this->Form->create();
                echo $this->Form->input('name', ['label' => __('Pavadinimas'), 'class' => 'form-control', 'id' => 'name', 'placeholder' => __('Kategorijos pavadinimas'), 'maxlength' => '100', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                
                echo $this->Form->submit(__('Pridėti'), ['class' => 'btn btn-default']);
                echo $this->Form->end(); 
            ?>
        </div>
        <div class="col-sm-3 col-xs-12">
            <h3><?= __('Kategorijos') ?></h3>
            <ul class="side-nav">
                <?php for ($i = 0; $i < sizeof($categories); $i++) { ?>
                    <li><?php echo $categories[$i]; ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
