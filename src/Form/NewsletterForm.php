<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class NewsletterForm extends Form {

    protected function _buildSchema(Schema $schema) {
        return $schema->addField('name', 'string')
            ->addField('email', ['type' => 'string'])
            ->addField('category', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
        return $validator->add('name', 'length', [
                'rule' => ['maxLength', 30],
                'message' => 'Vardas negali būti ilgesnis nei 30 simbolių.'
            ])->add('email', 'format', [
                'rule' => 'email',
                'message' => 'Elektroninis paštas neatitinka formato.',
            ]);
    }

    public function setErrors($errors) {
        $this->_errors = $errors;
    }
}
