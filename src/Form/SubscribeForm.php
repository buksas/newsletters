<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class NewsletterForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('name', 'string')
            ->addField('email', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator->add('name', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Privalote įvesti savo vardą.'
            ])->add('email', 'format', [
                'rule' => 'email',
                'message' => 'Elektroninis paštas neatitinka formato.',
            ]);
    }

    protected function _execute(array $data)
    {
        return true;
    }
}
