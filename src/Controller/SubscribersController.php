<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Xml;
use App\Form\NewsletterForm;

class SubscribersController extends AppController {

    public function form() {
        $this->layout = 'form';
        $cFile = $this->getFile('categories.xml');
        $catArray = $this->getArrayFromFile($cFile);
        $cFile->close();
        $newsletter = new NewsletterForm();
        if ($this->request->is('post')) {
            if (isset($this->request->data['category'])) {
                if ($newsletter->validate($this->request->data)) {
                    $sFile = $this->getFile('subscribers.xml');
                    $subscribersArray = $this->getArrayFromFile($sFile);
                    $subscriber = array('subscriber' => array('name' => $this->request->data['name'], 'email' => $this->request->data['email'], 'date' => date("Y-m-d H:i"), 'categories' => array('name' => $this->request->data['category'])));
                    $subscribersArray['subscribers']['subscriber'][sizeof($subscribersArray['subscribers']['subscriber'])] = $subscriber['subscriber'];
                    $xmlObject = Xml::build($subscribersArray);        
                    $xmlString = $xmlObject->asXML();
                
                    $sFile->write($xmlString);
                    $sFile->close();
                }
            } else {
                $this->set('catError', true);
            }           
        }
        
        $this->set('errors', $newsletter->errors());
        $this->set('newsletter', $newsletter);
        $this->set('categories', $catArray['categories']['name']);
    }
    
    public function index($sort = null) {
        $this->layout = 'form';
        $sFile = $this->getFile('subscribers.xml');
        $subscribersArray = $this->getArrayFromFile($sFile);
        $sFile->close();
        
        $this->set('subscribers', $subscribersArray['subscribers']['subscriber']);
    }
    
    private function getFile($fileName) {
        $dir = new Folder(ROOT);
        $file = new File($dir->pwd() . DS . $fileName);
        return $file;
    }
    
    private function getArrayFromFile($file) {
        $text = $file->read();
        $myXmlOriginal = $text;
        $xml = Xml::build($myXmlOriginal);
        $xmlArray = Xml::toArray($xml);
        return $xmlArray;
    }
}
