<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Xml;

class CategoriesController extends AppController {
    public function index() {
        $this->layout = 'form';
        $file = $this->getFile();
        $xmlArray = $this->getCategories($file);

        if ($this->request->is('post')) {
            array_push($xmlArray['categories']['name'], $this->request->data['name']);
            $xmlObject = Xml::build($xmlArray);        
            $xmlString = $xmlObject->asXML();
        
            $file->write($xmlString);
            $file->close();
            $this->Flash->success(__('Kategorija išsaugota.'));
        }
        
        $this->set('categories', $xmlArray['categories']['name']);
    }
    
    private function getFile() {
        $dir = new Folder(ROOT);
        $file = 'categories.xml';
        $file = new File($dir->pwd() . DS . $file);
        
        return $file;
    }
    
    private function getCategories($file) {
        $text = $file->read();
        $myXmlOriginal = $text;
        $xml = Xml::build($myXmlOriginal);
        $xmlArray = Xml::toArray($xml);
        
        return $xmlArray;
    }
}
