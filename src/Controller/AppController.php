<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller {

    public function initialize() {        
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Subscribers',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Subscribers',
                'action' => 'form',
                'home'
            ]
        ]);
    }
    
    public function beforeFilter(Event $event) {
        $this->Auth->allow(['form']);
    }
}
