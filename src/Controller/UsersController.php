<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController
{

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['logout']);
    }

    public function login() {
        if ($this->request->is('post')) {            
            if ($this->request->data['username'] == 'slaptas') {
                $this->Auth->setUser('slaptas');
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Prisijungti gali vartotojas, kurio prisijungimo vardas yra "slaptas"'));
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

}
